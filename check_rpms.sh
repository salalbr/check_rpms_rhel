#!/bin/bash
#
# Author: Maer Melo
#         salalbr [at] gmail [dot] com
#
# Usage: check_rpms.sh <input_file>

if [ `uname -p` == "x86_64" ]; then
  PLATFORM=rhel_6_64.in
elif [ `uname -p` == "i686" ]; then
  PLATFORM=rhel_6_32.in
else
  echo "Error: Unknown Processor Type!"
  exit 1
fi

for P in `cat $PLATFORM`
do
  rpm -q "$P" > t1
  my_size=$( wc -l < t1 )

    if [[ $( rpm -qa $P ) =~ ${P} ]]
    # if [[ $( rpm -qa $P ) == *${P}* ]]
    then
      echo "Package $P is installed."
    else
      echo "!!!Package $P NOT FOUND!!!"
    fi
done

rm -rf t1

exit 0
